import 'package:flutter/material.dart';

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:validators/validators.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:meta/meta.dart';

class savedCINs extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('Base de données '),
      backgroundColor: Colors.lightGreen,
      /*actions: <Widget>[
        IconButton(icon: Icon(Icons.list), onPressed: _pushSaved),
      ],*/
    ),
      body: StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('cin').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting: return new Text('Loading...');
          default:
            return new Container(
              child: ListView(
                children: snapshot.data.documents.map((DocumentSnapshot document) {
                  return new ListTile(
                    title: new Text(document['Nom']),
                    subtitle: new Text(document['Prénom']),
                    trailing: MaterialButton(
                      height: 10.0,
                      minWidth: 40.0,
                      color: Colors.white,
                        splashColor: Colors.green,
                        padding: EdgeInsets.all(1.2),
                        onPressed: () {String id= document['Nom']+'.'+document['Prénom'];
                        showData(context, id);

                        },

                        child: Icon(Icons.details, size: 15.0, color: Colors.lightGreen,  ),

                    )
                    /*Icon( Icons.details,
                      color: Colors.green,
                    )*/,
                  );
                }).toList(),
              ),
            );
        }
      },
    ),);
  }




  showData(BuildContext context, String id){
    //setState(() {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new savedData(id);
    }));
}

}





class savedData extends StatelessWidget{
  savedData(this.id);
  String id;


  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text('personne test '),
      backgroundColor: Colors.lightGreen,

    ),
      body: /*StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('cin').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError)
            return new Text('Error: ${snapshot.error}');
          switch (snapshot.connectionState) {
            case ConnectionState.waiting: return new Text('Loading...');
            default:
              return new Container(
                child: ListView(
                  children: snapshot.data.documents.map((DocumentSnapshot document) {
                    return new ListTile(
                      title: new Text(document['Nom']),
                      subtitle: new Text(document['Prénom']),
                    );
                  }).toList(),
                ),
              );
          }
        },
      ),*/
         new StreamBuilder(
        stream: Firestore.instance.collection('cin').document(id).snapshots(),
    builder: (context, snapshot) {
    if (!snapshot.hasData) {
    return new Text("Loading");
    }
    var userDocument = snapshot.data;
    return new Text(userDocument.toString());
    }
    ),
    );
  } }




