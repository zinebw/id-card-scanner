import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:validators/validators.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './savedCINs.dart';
//import './Data.dart';


class Processing extends StatelessWidget {

  var results;
  final Set<String> _savedCIN = Set<String>();

  Processing(this.lstRecto,this.lstVerso);
  List<TextLine> lstRecto;
  List<TextLine> lstVerso;



  bool isUppercase(String str) {
    return str == str.toUpperCase();
  }



  bool containsInt(String str){
    for(int i=0 ;i< str.length ; i++){
      if(isInt(str[i])){
        return true;
      }
    }
    return false;
  }



  bool cinNumber(String s) {
    if (s.length > 4) {
      if (isInt(s[2]) && isInt(s[3]) && isInt(s[4]) && !isInt(s[0]) && isAlpha(s[0])) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }


  void pickInfo(List<TextLine> lstRecto, List<TextLine> lstVerso){

    bool flagDate=false;
    bool flagPrenom=false;



    String numCin="";
    String nom="";
    String prenom="";
    String adresse="";
    String dateN="";
    String dateE="";
    String lieuN="";
    String nomP="";
    String nomM="";



    for (TextLine line in lstRecto) {


    if (((line.text.length>2 && isUppercase(line.text))&&(!(line.text.contains('.')))&&(!containsInt(line.text)) && (!line.text.contains('ROYAUME '))
    && (!line.text.contains('CARTE ')) )){
        if(flagPrenom==false){
          prenom=line.text;
          flagPrenom=true;
        }
        else nom=line.text;
      }

      
      if(line.text.contains('.')  ){
        if(flagDate==false){
          dateN=line.text;
          flagDate=true;
        }
        else dateE=line.text;
      }

      if(line.text.startsWith('à') ){
        lieuN=line.text.substring(2);
      }



      if (cinNumber(line.text)){
        numCin=line.text;
      }



    }



    for(TextLine line in lstVerso){
      if(line.text.contains('Fille de') || line.text.contains('Fils de')){
        nomP=line.text.substring(8);
      }
      if(line.text.contains('et de')){
        nomM=line.text.substring(6);
      }


      if(line.text.contains('Adresse')){
        if(line.text.length >= 9){
          adresse=line.text.substring(8);
        }
      }

    }

    results= {
      'Nom': nom,
      'Prénom': prenom,
      'N° CIN': numCin,
      'Date de naissance': dateN,
      'Lieu de naissance': lieuN,
      'Date d expiration': dateE,
      'Nom du père': nomP,
      'Nom de la mère': nomM,
      'Adresse': adresse
    };



    Firestore.instance.collection('cin').document(nom+'.'+prenom).setData(results);
    _savedCIN.add(nom+'.'+prenom);

    print(results.toString());
    print(_savedCIN);

  }


  // affichage des infos

 /* void showData() {
    const snapshot = await firebase.firestore().collection('events').get()
    return snapshot.docs.map(doc => doc.data());


    *//*Firestore.collection("categories").valueChanges().subscribe(data => {
    console.log(data);*//*
    // result should be: [{…}, {…}, {…}] ??

    }
*/

  showDatabase(BuildContext context){
    //setState(() {
      Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
        return new savedCINs();
      }));

  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(appBar: AppBar(
      title: Text('Etape 3: Confirmation '),
      backgroundColor: Colors.lightGreen,
      actions: <Widget>[
        IconButton(icon: Icon(Icons.list), onPressed: () {showDatabase(context);
        },
        ),
      ],
    ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 170.0,
              width: 220.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: ExactAssetImage('images/traitement.png'),
                      fit: BoxFit.cover)
              ),

            ),
            Padding(
              padding:EdgeInsets.all(40.0) ,
              child: Column(
                children: <Widget>[



                  RaisedButton(color: Colors.lightGreen,
                    splashColor: Colors.greenAccent,
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {pickInfo(lstRecto, lstVerso);
                    },
                    child: Row( // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.add_circle),
                        Text("         Traiter les informations    ", textAlign: TextAlign.center,),
                      ],
                    ),
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(70.70)),),
                  RaisedButton(color: Colors.lightGreen,
                    splashColor: Colors.greenAccent,
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {
                    /*Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
                      return new Data(lstRecto,lstVerso);
                    }));*/
                    },

                    child: Row( // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.description ),
                        Text("         Afficher le résultat    ", textAlign: TextAlign.center,) 
                      ],
                    ),
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(70.70)),),
                  //InformationList(results),
                ],
              ),
            ),

          ],
          // This trailing comma makes auto-formatting nicer for build methods.
        ),)
      ,);
  }



  void _pushSaved() {
    /*Navigator.of(context).push(new
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _savedCIN.map(
                (Strin<g person) {
              return ListTile(
                title: Text(
                  person,
                ),
              );
            },
          );
          final List<Widget> divided = ListTile
              .divideTiles(
            context: context,
            tiles: tiles,
          )
              .toList();
          return Scaffold(
            appBar: AppBar(
              title: Text('Saved CINs'),
            ),
            body: ListView(children: divided),
          );
        },
      ),
    );*/
  }
}
