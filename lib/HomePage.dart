import 'package:flutter/material.dart';
import './ScanRecto.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';

void main()async => runApp(new MaterialApp(home:MyApp()) );

class MyApp extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(appBar: AppBar(

      title: Text('ID Scanner'),
      backgroundColor: Colors.lightGreen,
    ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Bienvenue!', textAlign: TextAlign.center,style:new TextStyle(
              fontSize: 30.0,
              color: Colors.lightGreen,
                fontWeight: FontWeight.bold,
            ) ,),
            Center(
              child: Container(
                  height: 200.0,
                  width: 200.0,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: ExactAssetImage('images/ocpScan.png'),
                           fit: BoxFit.cover)
                  )
              ),
            ),
            Text('Veuillez commencer par scanner le recto de votre carte puis passez au verso, merci. ', textAlign: TextAlign.center,style:new TextStyle(
              fontSize: 20.0,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ) , ),
            RaisedButton(color: Colors.lightGreen,
              splashColor: Colors.greenAccent,

              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ScanRecto()),
                );
              },
              child: Icon(Icons.arrow_right),
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(80.80)),
            ),

          ],
          // This trailing comma makes auto-formatting nicer for build methods.
        ),)
      ,);
  }
}
