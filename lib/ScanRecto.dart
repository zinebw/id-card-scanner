import 'package:flutter/material.dart';
//import './ProcessingRecto.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:validators/validators.dart';
import 'package:zineb_test/savedCINs.dart';
import './ScanVerso.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';



// ------------------------------------------    PARTIE STATELESS      ------------------------------------------

class ScanRecto extends StatelessWidget{
  ScanRecto();


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(

        title: Text('ETAPE 1 : Recto '),
        backgroundColor: Colors.lightGreen,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.list), onPressed: () {showDatabase(context);
          },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Container(
                  height: 120.0,
                  width: 180.0,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: ExactAssetImage('images/idRectoL.png'),
                          fit: BoxFit.cover)
                  )
              ),
            ),
            Padding(
              padding:EdgeInsets.all(50.0) ,
              child: Text("Veuillez choisir la source de l'image :", textAlign: TextAlign.center,style:new TextStyle(
                fontSize: 15.0,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ) ,
              ),
            ),

            ProcessingRecto('camera'),
            ProcessingRecto('galerie'),
          ],
          // This trailing comma makes auto-formatting nicer for build methods.
        ),),);
  }
}


showDatabase(BuildContext context){
  //setState(() {
  Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
    return new savedCINs();
  }));}

// ------------------------------------------    PARTIE STATEFUL     ------------------------------------------


class ProcessingRecto extends StatefulWidget{

  final String mode;
  ProcessingRecto(this.mode);

  @override
  State<StatefulWidget> createState() {
    return _ProcessingRectoState();
  }}


class _ProcessingRectoState extends State<ProcessingRecto>{
  String _source;


  List<TextLine> _lstRecto =[] ;




  @override
  void initState() {
    _source=widget.mode;
    super.initState();
  }
  @override
  void didUpdateWidget(ProcessingRecto oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  IconData iconChoice(){
    if(_source=='camera') {
      return Icons.add_a_photo;
    }
    else{
      return Icons.add_photo_alternate;
    }
  }


  String source(){
    if(_source=='camera'){
      return '    Prendre une photo      ';
    }
    else{
      return '    Choisir une photo      ';
    }
  }
  Future takeImage()async {
    File pickedImageRecto;
    bool isImageLoaded= false;

    try  {
      if (_source == 'camera') {
        final File imageFile = await ImagePicker.pickImage(
          source: ImageSource.camera,);
        File croppedFile = await ImageCropper.cropImage(
          sourcePath: imageFile.path,
          ratioX: 1.0,
          ratioY: 0.63,
        );


        final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(
            croppedFile);
        pickedImageRecto = croppedFile;
        isImageLoaded = true;
        final TextRecognizer textRecognizer = FirebaseVision.instance
            .textRecognizer();
        final VisionText visionText = await textRecognizer.processImage(
            visionImage);
        for (TextBlock block in visionText.blocks) {
          for (TextLine line in block.lines) {
            _lstRecto.add(line);

          }
        }
        setState(() {
          Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
            return new ScanVerso(_lstRecto);
          }));
        });
      }
      else{
        final File imageFile = await ImagePicker.pickImage(
          source: ImageSource.gallery,);
        File croppedFile = await ImageCropper.cropImage(
          sourcePath: imageFile.path,
          ratioX: 1.0,
          ratioY: 0.63,
        );


        final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(
            croppedFile);
        pickedImageRecto = croppedFile;
        isImageLoaded = true;
        final TextRecognizer textRecognizer = FirebaseVision.instance
            .textRecognizer();
        final VisionText visionText = await textRecognizer.processImage(
            visionImage);
        for (TextBlock block in visionText.blocks) {
          for (TextLine line in block.lines) {
            _lstRecto.add(line);
          }
        }


        setState(() {
          Navigator.of(context).push(new MaterialPageRoute(builder: (context){return new ScanVerso(_lstRecto);}));
        });
      }
    }
    catch(e) {
      print(e);
    }

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(margin: EdgeInsets.all(20.0),
      child: SizedBox(width: 240.0,
        height: 65.0,
        child: RaisedButton(color: Colors.lightGreen,
          splashColor: Colors.greenAccent,
          onPressed: () {takeImage();

          },
          child: Row(children: <Widget>[Icon(iconChoice()), Text(source(), textAlign: TextAlign.center),],
          ),
          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(80.80)),
        ),
      ),
    );
  }}








