import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:validators/validators.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './Processing.dart';


class Information extends StatelessWidget {
  Information(this.lstRecto,this.lstVerso);
  List<TextLine> lstRecto;
  List<TextLine> lstVerso;
  var results;





  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(appBar: AppBar(
      title: Text('Etape 3: Confirmation '),
      backgroundColor: Colors.lightGreen,
    ),
      body:Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[Text('--  Insérer les images ou afficher les informatiosn --',textAlign: TextAlign.center,style:new TextStyle(
            fontSize: 15.0,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ) ,),
            Padding(
              padding:EdgeInsets.all(40.0) ,
              child: Column(
                children: <Widget>[
                  RaisedButton(color: Colors.lightGreen,
                    splashColor: Colors.greenAccent,
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {
                    },
                    child: Row( // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.add_circle),
                        Text("         Traiter les informations    ", textAlign: TextAlign.center,),
                      ],
                    ),
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(70.70)),),
                  RaisedButton(color: Colors.lightGreen,
                    splashColor: Colors.greenAccent,
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {//showData();
                    },

                    child: Row( // Replace with a Row for horizontal icon + text
                      children: <Widget>[
                        Icon(Icons.description ),
                        Text("         Afficher le résultat    ", textAlign: TextAlign.center,)
                      ],
                    ),
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(70.70)),),
                ],
              ),
            ),

          ],
          // This trailing comma makes auto-formatting nicer for build methods.
        ),)
      ,);
  }
}
