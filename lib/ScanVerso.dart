import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:image_cropper/image_cropper.dart';
import './Processing.dart';
import 'savedCINs.dart';
//import './ProcessingVerso.dart';
//import 'dart:io';

// ------------------------------------------    PARTIE STATELESS      ------------------------------------------
class ScanVerso extends StatelessWidget{

  ScanVerso(this.lstVerso);
  List<TextLine> lstVerso =[] ;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('ETAPE 2 : Verso '),
        backgroundColor: Colors.lightGreen,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.list), onPressed: () {
            showDatabase(context);
          },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[
            Center(
              child: Container(
                  height: 120.0,
                  width: 185.0,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: ExactAssetImage('images/idVersoL.png'),
                          fit: BoxFit.cover)
                  )
              ),
            ),

            Padding(
              padding: EdgeInsets.all(50.0),
              child: Text("Veuillez choisir la source de l'image : ",
                textAlign: TextAlign.center, style: new TextStyle(
                  fontSize: 15.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),),
            ),
            ProcessingVerso('camera', lstVerso),
            ProcessingVerso('galerie', lstVerso),
          ],
          // This trailing comma makes auto-formatting nicer for build methods.
        ),),);
  }
}

showDatabase(BuildContext context){
  Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
    return new savedCINs();
  }));}



// ------------------------------------------    PARTIE STATEFUL     ------------------------------------------



class ProcessingVerso extends StatefulWidget{
  List<TextLine> lstRecto =[] ;



  final String mode;
  File pickedImageRecto;


  ProcessingVerso(this.mode,this.lstRecto);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProcessingVersoState();
  }}

class _ProcessingVersoState extends State<ProcessingVerso>{


  List<TextLine> _lstRecto =[] ;
  List<TextLine> _lstVerso =[] ;
  String _source;



  @override
  void initState() {
    _source=widget.mode;
    _lstRecto=widget.lstRecto;
    super.initState();
  }
  @override
  void didUpdateWidget(ProcessingVerso oldWidget) {
    super.didUpdateWidget(oldWidget);
  }



  IconData iconChoice(){
    if(_source=='camera') {
      return Icons.add_a_photo;
    }
    else{
      return Icons.add_photo_alternate;
    }
  }


  String source(){
    if(_source=='camera'){
      return '    Prendre une photo  ';}
    else{
      return '    Choisir une photo   ';}
  }

  Future takeImage() async {
    try {
      if (_source == 'camera') {
        final File imageFile = await ImagePicker.pickImage(
          source: ImageSource.camera,);
        File croppedFile = await ImageCropper.cropImage(
          sourcePath: imageFile.path,
          ratioX: 1.0,
          ratioY: 0.63,
          //maxWidth: 1000,
          //maxHeight: 1000,
        );
        final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(
            croppedFile);
        final TextRecognizer textRecognizer = FirebaseVision.instance
            .textRecognizer();
        final VisionText visionText = await textRecognizer.processImage(
            visionImage);
        for (TextBlock block in visionText.blocks) {
          for (TextLine line in block.lines) {
            _lstVerso.add(line);
          }
        }
        setState(() {
          Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
            return new Processing(_lstRecto, _lstVerso);
          }));
        });
      }
      else {
        final File imageFile = await ImagePicker.pickImage(
          source: ImageSource.gallery,);
        File croppedFile = await ImageCropper.cropImage(
          sourcePath: imageFile.path,
          ratioX: 1.0,
          ratioY: 0.63,
        );
        final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(
            croppedFile);
        final TextRecognizer textRecognizer = FirebaseVision.instance
            .textRecognizer();
        final VisionText visionText = await textRecognizer.processImage(
            visionImage);
        for (TextBlock block in visionText.blocks) {
          for (TextLine line in block.lines) {
            _lstVerso.add(line);
          }
        }
        setState(() {
          Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
            return new Processing(_lstRecto, _lstVerso,);
          }));
        }
        );
      }
    }
    catch (e) {
      print(e);
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(margin: EdgeInsets.all(20.0),
      child: SizedBox(width: 240.0,
        height: 65.0,
        child: RaisedButton(
          color: Colors.lightGreen,
          splashColor: Colors.greenAccent,
          onPressed: () {
            takeImage();
          },
          child: Row(children: <Widget>[
            Icon(iconChoice()), Text(source(), textAlign: TextAlign.center),
          ],
          ),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(80.80)),
        ),
      ),);
  }
}
